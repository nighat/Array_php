<?php
echo __LINE__."<hr>";
echo __FILE__."<hr>";
echo __DIR__."<hr>";
function doSomething(){
    echo __FUNCTION__."<hr>";
}
doSomething();

echo "<hr>";
class MyClass{
    use MyTrait;
    public function  doSomethingMethod(){
        echo __CLASS__;
    }
}
$obj=new MyClass();
$obj->doSomethingMethod();
$obj->MyTrait();

trait  MyTrait{
    public function processSth(){
        echo __TRAIT__;
    }
}
echo __NAMESPACE__;
echo "<hr>";